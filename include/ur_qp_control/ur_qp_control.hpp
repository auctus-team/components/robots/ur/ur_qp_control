#pragma once

#include "pinocchio/fwd.hpp"
#include <ros/ros.h>
#include <ros/node_handle.h>
#include <ros/time.h>

#include <ros/package.h>

#include <velocity_qp/controller.h>
#include "sensor_msgs/JointState.h"
#include "gazebo_msgs/GetPhysicsProperties.h"
#include <moveit_trajectory_interface/moveit_trajectory_interface.hpp>
#include <chrono>
#include <thread>

Eigen::Matrix<double,6,1> q,qd;
Eigen::Matrix<double,6,1> update();
sensor_msgs::JointState sortJointState(sensor_msgs::JointState::ConstPtr msg);
// void JointStateCallback(const sensor_msgs::JointState::ConstPtr& msg);

bool got_first_state = false;

bool initialized = false;
QPController::Velocity velocity_qp;
// Trajectory variables
std::unique_ptr<TrajectoryInterface> trajectory;
std::string controlled_frame;
Eigen::VectorXd p_gains;
// Pinocchio Model and Data
pinocchio::Model model;
pinocchio::Data data;
std::vector<std::string> joint_names;
ros::Publisher joint_velocity_command_pub;
std_msgs::Float64MultiArray joint_command_msg;
gazebo_msgs::GetPhysicsProperties srv;
std::string robot_name = "ur3_robot";