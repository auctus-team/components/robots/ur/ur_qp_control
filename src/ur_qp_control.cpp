#include <ur_qp_control/ur_qp_control.hpp>


void JointStateCallback(const sensor_msgs::JointState::ConstPtr& msg)
{
    sensor_msgs::JointState sorted_joint_state = sortJointState(msg);
    for (int i=0 ; i<6; ++i)
    {
        q(i) = sorted_joint_state.position[i];
        qd(i) = sorted_joint_state.velocity[i]; 
    }

    if (initialized)
    {
        Eigen::Matrix<double,6,1> joint_command = update();
        
        tf::matrixEigenToMsg(joint_command,joint_command_msg);
        joint_velocity_command_pub.publish(joint_command_msg);
    }
    got_first_state = true;
}

sensor_msgs::JointState sortJointState(sensor_msgs::JointState::ConstPtr msg)
{
    // This function is used to sort the joint comming from the /joint_state topic
    // They are order alphabetically while we need them to be order as in the URDF file
    // With use the joint_names variable that defined the joints in the correct order
    sensor_msgs::JointState sorted_joint_state = *msg;
    std::vector<std::string> joint_names_from_urdf;
    for(pinocchio::JointIndex joint_id = 1; joint_id < (pinocchio::JointIndex)model.njoints; ++joint_id)
    {
      joint_names_from_urdf.push_back(model.names[joint_id]);
    }
    int i = 0;
    for (auto ith_unsorted_joint_name: msg->name)
    {
        int j=0;
        for (auto jth_joint_name : joint_names_from_urdf)    
        {
            if(ith_unsorted_joint_name.find(jth_joint_name) != std::string::npos)
            {
                sorted_joint_state.position[j] = msg->position[i];
                sorted_joint_state.velocity[j] = msg->velocity[i];
                sorted_joint_state.name[j] = ith_unsorted_joint_name;
                break;
            }
            else
            {
                j++;
            }
        }
        i++;
    }

    return sorted_joint_state;
}


bool initialize()
{
    ROS_WARN_STREAM_ONCE("Initializing UR QP Controller");
    if (!got_first_state)
        return false;
    
    std::string robot_description;
    ros::param::get(robot_name+"/robot_description", robot_description);
    p_gains.resize(6);
    std::vector<double> p_gains_std;
    ros::param::get(robot_name+"/velocity_control/p_gains", p_gains_std);
     for (int i = 0; i < p_gains.size(); ++i)
            p_gains(i) = p_gains_std[i];
    std::string move_group;
    ros::param::get(robot_name+"/velocity_control/move_group", move_group);

    if (!initialized) 
    {
        velocity_qp.setControlledFrame("tool0");
        velocity_qp.init(robot_description,joint_names) ;
        velocity_qp.setControlPeriod(0.008);
        velocity_qp.setRegularizationWeight(0.0);
        ROS_WARN_STREAM("Initialized qp");
        trajectory->interface->setControlledFrame("tool0");
        trajectory->interface->setTrajectoryTimeIncrement(0.008);
        trajectory->interface->init(move_group,robot_name+"/robot_description",q,joint_names);
        ROS_WARN_STREAM("Initialized trajectory");
        ROS_WARN_STREAM("velocity_qp.getControlledFrame() : " << velocity_qp.getControlledFrame()); 
        model = velocity_qp.getRobotModel();
        data = pinocchio::Data(model);
    }
    ROS_WARN_STREAM("UR_QP_Control initialized");
    return true;
}

Eigen::Matrix<double,6,1> update()
{
    // ROS_WARN_STREAM_THROTTLE(1.0,"Updating");
    // Update model
    // First calls the forwardKinematics on the model, then computes the placement of each frame.
    pinocchio::forwardKinematics(model,data,q);
    pinocchio::updateFramePlacements(model,data);

    //Update the trajectory
    trajectory->interface->updateTrajectory();
    pinocchio::SE3 oMdes(trajectory->interface->getCartesianPose().matrix());
    // ROS_WARN_STREAM("oMdes\n" << oMdes.toHomogeneousMatrix());
    // ROS_WARN_STREAM("oMcurr\n" <<  data.oMf[model.getFrameId(velocity_qp.getControlledFrame())].toHomogeneousMatrix());
    // Compute error
    const pinocchio::SE3 tipMdes = data.oMf[model.getFrameId(velocity_qp.getControlledFrame())].actInv(oMdes);
    Eigen::Matrix<double,6,1> err = pinocchio::log6(tipMdes).toVector();
    // Proportional controller with feedforward
    Eigen::Matrix<double,6,1> xd_star = p_gains.cwiseProduct(err); //+ trajectoryInterface.getCartesianVelocity();
    // ROS_WARN_STREAM("xd_star\n" <<  xd_star);

    Eigen::Matrix<double,6,1> joint_command = velocity_qp.update(q,qd,xd_star);

    // Send joint commands
    return joint_command;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "ur_qp_control");
    ROS_WARN_STREAM_ONCE("Starting main UR QP Controller");
    trajectory.reset(new TrajectoryInterface());
    ros::NodeHandle n("~");
    ros::Rate loop_rate(125.0);
    bool sim;
    n.getParam("robot", robot_name);
    robot_name = "/" + robot_name;
    ROS_WARN_STREAM_ONCE("Robot Name :" << robot_name);
    ros::param::get(robot_name+"/sim", sim);
    if (sim)
    {
        // Hack to wait for gazebo to be ready before initiating the robot state
        ros::ServiceClient gazebo_client = n.serviceClient<gazebo_msgs::GetPhysicsProperties>(robot_name+"/gazebo/get_physics_properties");
        ROS_WARN_STREAM_ONCE("Waiting for gazebo to be ready");
        gazebo_client.waitForExistence();
        std::this_thread::sleep_for(std::chrono::milliseconds(5000)); // sleep to wait for joint to be in correct configuration 

        ROS_WARN_STREAM_ONCE("Gazebo ready");
    }

    ros::Subscriber joint_state_sub = n.subscribe(robot_name+"/joint_states", 1000, JointStateCallback);
    joint_velocity_command_pub = n.advertise<std_msgs::Float64MultiArray>(robot_name+"/joint_group_vel_controller/command", 1000);
    ros::param::get(robot_name+"/velocity_control/joint_names", joint_names);
    while(ros::ok())
    {
        if(!initialized){
            ROS_WARN_STREAM_ONCE("Wait for init");
            initialized = initialize();
        }

        ros::spinOnce();

        loop_rate.sleep();
    }
    return 0;
}